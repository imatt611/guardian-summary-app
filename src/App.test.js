import waitUntil from 'async-wait-until';
import { shallow } from 'enzyme';
import { Heading } from 'grommet';
import React from 'react';
import sampleArticles from '../apiSample_10Articles.json';
import App from './App.js';
import ArticleList from './ArticleList.js';
import SearchField from './SearchField.js';

describe('App render', () => {

  const testQuery = 'foo';
  const sampleArticlesSubset = sampleArticles.slice(sampleArticles.length - 2);
  const fakeArticlesProvider = async opts => {
    if (opts && testQuery === opts.query) return sampleArticlesSubset;
    return sampleArticles;
  };
  const appWrapper = shallow(<App articlesProvider={fakeArticlesProvider}/>);

  it('should render a list of articles', () => {
    expect(appWrapper.find(ArticleList).getElements().length).toEqual(1);
  });

  it('should render the page title', () => {
    const headingWrapper = appWrapper.find(Heading);
    expect(headingWrapper.props().children)
      .toEqual('Latest News from The Guardian');
    expect(headingWrapper.getElement()).toMatchSnapshot();
  });

  it('should render a search field with a provided change handler', () => {
    const searchFieldWrapper = appWrapper.find(SearchField);
    expect(searchFieldWrapper.getElements().length).toEqual(1);
    expect(searchFieldWrapper.getElement().props.onChange)
      .toBeInstanceOf(Function);
  });

  it('should respond to changes in search field', async () => {
    let searchFieldWrapper = appWrapper.find(SearchField);
    const changeHandler = searchFieldWrapper.getElement().props.onChange;
    const fakeEvent = {target: {value: testQuery}};
    await changeHandler(fakeEvent);
    searchFieldWrapper = appWrapper.find(SearchField);
    return waitUntil(
      () => searchFieldWrapper.props().value === testQuery)
      .then(() => {
        expect(searchFieldWrapper.props().value).toEqual(testQuery);
      });
  });
});
