import fetchMock from 'fetch-mock';
import sampleArticles from '../apiSample_10Articles.json';
import { API_KEY__GUARDIAN } from './config';
import { fetchArticles } from './News.js';


describe(`News fetching`, function () {

  const sampleDataWrapper = new Promise(
    res => res({
      response: {
        results: sampleArticles,
      },
    }));

  beforeAll(() => {
    fetchMock.config.overwriteRoutes = true;
  });

  afterEach(fetchMock.resetHistory);

  afterAll(fetchMock.restore);

  it('should request the newest 10 articles using api key', () => {
    fetchMock.mock(
      'begin:https://content.guardianapis.com/search', sampleDataWrapper, {
        'order-by': 'newest',
        'use-date': 'published',
        'page-size': 10,
        'page': 1,
        'api-key': API_KEY__GUARDIAN,
      });
    return fetchArticles().then(() => {
      expect(fetchMock.called()).toBe(true);
    });
  });

  it('should return results array on success', () => {
    fetchMock.mock(
      'begin:https://content.guardianapis.com/search', sampleDataWrapper);
    return fetchArticles().then(articles => {
      expect(fetchMock.called()).toBe(true);
      expect(articles).toEqual(sampleArticles);
    });
  });
});
