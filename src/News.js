import { API_KEY__GUARDIAN } from './config';

const baseUrl = `https://content.guardianapis.com`;
const endpointSearch = `/search`;
const defaultQueryParams = `order-by=newest&use-date=published&page-size=10`;

const fetchArticles = ({page = 1, query} = {}) => {
  const q = query ? `q=${query}&` : '';
  return fetch(
    `${baseUrl}${endpointSearch}?${q}${defaultQueryParams}&page=${page}&api-key=${API_KEY__GUARDIAN}`)
    .then(res => res.json())
    .then(json => json.response.results);
};

export {
  fetchArticles,
};
