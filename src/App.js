import { Box, Grommet, Heading } from 'grommet';
import React, { Component } from 'react';
import ArticleList from './ArticleList';
import { fetchArticles } from './News';
import SearchField from './SearchField.js';

const theme = {
  global: {
    font: {
      family: 'Roboto',
      size: '14px',
      height: '20px',
    },
  },
};

class App extends Component {
  constructor(props) {
    super(props);

    this.articlesProvider = props.articlesProvider || fetchArticles;
    this.handleSearchChange =
      async event => {
        this.setState({
          searchValue: event.target.value,
        });
        this.setState({
          articles: await this.articlesProvider({query: event.target.value}),
        });
      };

    this.state = {
      articles: [],
      searchValue: '',
    };
  }

  componentDidMount() {
    this.articlesProvider()
      .then(articles => this.setState({articles: articles}));
  }

  render() {
    return (
      <Grommet theme={theme} full>
        <Box fill background={'light-3'}>
          <Box flex direction={'column'} align={'center'}
               overflow={{horizontal: 'hidden'}}>
            <Box align={'center'} justify={'center'}>
              <Heading level={1} size={'medium'} color={'dark-2'}>Latest News
                from The Guardian</Heading>
              <Box width={'300px'}>
                <SearchField value={this.state.searchValue}
                             onChange={this.handleSearchChange}
                             size={'medium'}/>
              </Box>
              <ArticleList articles={this.state.articles} margin={'medium'}/>
            </Box>
          </Box>
        </Box>
      </Grommet>
    );
  }
}

export default App;
