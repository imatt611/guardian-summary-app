import { TextInput } from 'grommet';
import React from 'react';

export default props => (
  <TextInput placeholder={'Type to search'}
             name={'Search'}
             {...props} />
);
