import { fetchArticles } from './News.js';

const getDate24HoursAgo = () => {
  const date = new Date();
  date.setHours(date.getHours() - 24);
  return date;
};

describe(`Integration with The Guardian's search API`, function () {

  it('should retrieve the newest 10 articles', () => {
    const articlesPromise = fetchArticles();
    const twentyFourHoursAgo = getDate24HoursAgo();

    // Can't prove "newest", so use approximation of "published within last 24
    // hours"
    return articlesPromise.then(articles => {
      expect(articles.filter(
        a => new Date(a.webPublicationDate) >= twentyFourHoursAgo).length)
        .toEqual(10);
    });
  });

  it('should get next 10 articles by requested page', () => {
    const oldestPageOnePubDatePromise = fetchArticles({page: 1})
      .then(articles => articles.reduce((earliestPubDate, article) => {
        const pubDate = new Date(article.webPublicationDate);
        return pubDate < earliestPubDate ? pubDate : earliestPubDate;
      }, new Date()));

    return Promise.all([oldestPageOnePubDatePromise, fetchArticles({page: 2})])
      .then(([oldestPageOnePubDate, pageTwoArticles]) => expect(
        pageTwoArticles.filter(
          a => new Date(a.webPublicationDate) <= oldestPageOnePubDate).length)
        .toEqual(10));
  });

  /**
   * Validates that a query was used without having to spy. This avoidance of
   * implementation knowledge is at the cost of theoretical false positives.
   * The premise here is that none of the articles found in the second page of
   * a specific query would be present in the first page of retrieval without a
   * query. This logic depends on the publication date order. The test could
   * yield a false positive if the non-queried request contained one or more
   * articles that match the query and between the instant of the non-queried
   * request and the queried request, enough new articles were published that
   * match the query that one which had been in the most recent of the
   * non-queried articles would now be in the second page of the queried
   * articles.
   *
   * One hopes, then, that there is never a flood of news about
   * France that The Guardian publishes at once in a large enough set to
   * disrupt these assumptions at exactly the moment this test is executed.
   *
   * Additionally, the tested query could have no second page at all (which
   * would be much more likely if the test query were to be changed to
   * something unlikely to have 10 articles retrievable through The Guardian's
   * API), in which case the positive result from this test is essentially
   * valid--the query would still have yielded entirely different results than
   * the control--but risks being a false positive if the implementation were
   * adjusted to return an empty array any time a query string is passed to it.
   */
  it('should get articles according to a query', () => {
    return Promise.all(
      [fetchArticles({page: 1}), fetchArticles({query: 'France', page: 2})])
      .then(([nonqueried, queried]) => {
        const nqIds = nonqueried.map(a => a.id);
        const qIds = queried.map(a => a.id);
        const countNotMatched = nqIds.slice(0).reduce((acc, val, i, arr) => {
          if (qIds.includes(val)) arr.splice(i - 1);
          return acc + 1;
        }, 0);
        expect(countNotMatched).toEqual(nqIds.length);
      });
  });
});
