import { Text } from 'grommet';
import React from 'react';

export default props => {
  const {webUrl, webTitle} = props.data;
  return (
    <Text>
      <a href={webUrl} target={'_blank'}>{webTitle}</a>
    </Text>
  );
}
